# -*- mode: python; -*-
import atexit
import copy
import datetime
import errno
import json
import os
import re
import shlex
import shutil
import stat
import sys
import textwrap
import uuid
from buildscripts import utils
from buildscripts import moduleconfig

import SCons

from mongo_scons_utils import (
    default_buildinfo_environment_data,
    default_variant_dir_generator,
    get_toolchain_ver,
)

import libdeps

EnsureSConsVersion( 2, 3, 0 )

def print_build_failures():
    from SCons.Script import GetBuildFailures
    for bf in GetBuildFailures():
        print "%s failed: %s" % (bf.node, bf.errstr)
atexit.register(print_build_failures)

def versiontuple(v):
    return tuple(map(int, (v.split("."))))

# --- OS identification ---
#
# This needs to precede the options section so that we can only offer some options on certain
# operating systems.

# This function gets the running OS as identified by Python
# It should only be used to set up defaults for options/variables, because
# its value could potentially be overridden by setting TARGET_OS on the
# command-line. Treat this output as the value of HOST_OS
def get_running_os_name():
    running_os = os.sys.platform
    if running_os.startswith('linux'):
        running_os = 'linux'
    elif running_os.startswith('freebsd'):
        running_os = 'freebsd'
    elif running_os.startswith('openbsd'):
        running_os = 'openbsd'
    elif running_os == 'sunos5':
        running_os = 'solaris'
    elif running_os == 'win32':
        running_os = 'windows'
    elif running_os == 'darwin':
        running_os = 'osx'
    else:
        running_os = 'unknown'
    return running_os

def env_get_os_name_wrapper(self):
    return env['TARGET_OS']

def is_os_raw(target_os, os_list_to_check):
    okay = False
    posix_os_list = [ 'linux', 'openbsd', 'freebsd', 'osx', 'solaris' ]

    for p in os_list_to_check:
        if p == 'posix' and target_os in posix_os_list:
            okay = True
            break
        elif p == target_os:
            okay = True
            break
    return okay

# This function tests the running OS as identified by Python
# It should only be used to set up defaults for options/variables, because
# its value could potentially be overridden by setting TARGET_OS on the
# command-line. Treat this output as the value of HOST_OS
def is_running_os(*os_list):
    return is_os_raw(get_running_os_name(), os_list)

def env_os_is_wrapper(self, *os_list):
    return is_os_raw(self['TARGET_OS'], os_list)

def add_option(name, **kwargs):

    if 'dest' not in kwargs:
        kwargs['dest'] = name

    if 'metavar' not in kwargs and kwargs.get('type', None) == 'choice':
        kwargs['metavar'] = '[' + '|'.join(kwargs['choices']) + ']'

    AddOption('--' + name, **kwargs)

def get_option(name):
    return GetOption(name)

def has_option(name):
    optval = GetOption(name)
    # Options with nargs=0 are true when their value is the empty tuple. Otherwise,
    # if the value is falsish (empty string, None, etc.), coerce to False.
    return True if optval == () else bool(optval)

def use_system_version_of_library(name):
    return has_option('use-system-all') or has_option('use-system-' + name)

# Returns true if we have been configured to use a system version of any C++ library. If you
# add a new C++ library dependency that may be shimmed out to the system, add it to the below
# list.
def using_system_version_of_cxx_libraries():
    cxx_library_names = ["tcmalloc", "boost"]
    return True in [use_system_version_of_library(x) for x in cxx_library_names]

def make_variant_dir_generator():
    memoized_variant_dir = [False]
    def generate_variant_dir(target, source, env, for_signature):
        if not memoized_variant_dir[0]:
            memoized_variant_dir[0] = env.subst('$BUILD_ROOT/$VARIANT_DIR')
        return memoized_variant_dir[0]
    return generate_variant_dir

# Options TODOs:
#
# - We should either alphabetize the entire list of options, or split them into logical groups
#   with clear boundaries, and then alphabetize the groups. There is no way in SCons though to
#   inform it of options groups.
#
# - Many of these options are currently only either present or absent. This is not good for
#   scripting the build invocation because it means you need to interpolate in the presence of
#   the whole option. It is better to make all options take an optional on/off or true/false
#   using the nargs='const' mechanism.
#

add_option('prefix',
    default='$BUILD_ROOT/install',
    help='installation prefix',
)

add_option('nostrip',
    help='do not strip installed binaries',
    nargs=0,
)

add_option('build-dir',
    default='#build',
    help='build output directory',
)

add_option('release',
    help='release build',
    nargs=0,
)

add_option('lto',
    help='enable link time optimizations (experimental, except with MSVC)',
    nargs=0,
)

add_option('dynamic-windows',
    help='dynamically link on Windows',
    nargs=0,
)

add_option('endian',
    choices=['big', 'little', 'auto'],
    default='auto',
    help='endianness of target platform',
    nargs=1,
    type='choice',
)

add_option('disable-minimum-compiler-version-enforcement',
    help='allow use of unsupported older compilers (NEVER for production builds)',
    nargs=0,
)

add_option('ssl',
    help='Enable SSL',
    nargs=0
)

add_option('mmapv1',
    choices=['auto', 'on', 'off'],
    default='auto',
    help='Enable MMapV1',
    nargs='?',
    type='choice',
)

add_option('wiredtiger',
    choices=['on', 'off'],
    const='on',
    default='on',
    help='Enable wiredtiger',
    nargs='?',
    type='choice',
)

js_engine_choices = ['mozjs', 'none']
add_option('js-engine',
    choices=js_engine_choices,
    default=js_engine_choices[0],
    help='JavaScript scripting engine implementation',
    type='choice',
)

add_option('server-js',
    choices=['on', 'off'],
    const='on',
    default='on',
    help='Build mongod without JavaScript support',
    type='choice',
)

add_option('libc++',
    help='use libc++ (experimental, requires clang)',
    nargs=0,
)

add_option('use-glibcxx-debug',
    help='Enable the glibc++ debug implementations of the C++ standard libary',
    nargs=0,
)

add_option('noshell',
    help="don't build shell",
    nargs=0,
)

add_option('safeshell',
    help="don't let shell scripts run programs (still, don't run untrusted scripts)",
    nargs=0,
)

add_option('dbg',
    choices=['on', 'off'],
    const='on',
    default='off',
    help='Enable runtime debugging checks',
    nargs='?',
    type='choice',
)

add_option('spider-monkey-dbg',
    choices=['on', 'off'],
    const='on',
    default='off',
    help='Enable SpiderMonkey debug mode',
    nargs='?',
    type='choice',
)

add_option('opt',
    choices=['on', 'off'],
    const='on',
    help='Enable compile-time optimization',
    nargs='?',
    type='choice',
)

add_option('sanitize',
    help='enable selected sanitizers',
    metavar='san1,san2,...sanN',
)

add_option('llvm-symbolizer',
    default='llvm-symbolizer',
    help='name of (or path to) the LLVM symbolizer',
)

add_option('durableDefaultOn',
    help='have durable default to on',
    nargs=0,
)

add_option('allocator',
    choices=["auto", "system", "tcmalloc"],
    default="auto",
    help='allocator to use (use "auto" for best choice for current platform)',
    type='choice',
)

add_option('gdbserver',
    help='build in gdb server support',
    nargs=0,
)

add_option('gcov',
    help='compile with flags for gcov',
    nargs=0,
)

add_option('smokedbprefix',
    help='prefix to dbpath et al. for smoke tests',
)

add_option('smokeauth',
    help='run smoke tests with --auth',
    nargs=0,
)

add_option('use-sasl-client',
    help='Support SASL authentication in the client library',
    nargs=0,
)

add_option('use-system-tcmalloc',
    help='use system version of tcmalloc library',
    nargs=0,
)

add_option('use-system-pcre',
    help='use system version of pcre library',
    nargs=0,
)

add_option('use-system-wiredtiger',
    help='use system version of wiredtiger library',
    nargs=0,
)

add_option('system-boost-lib-search-suffixes',
    help='Comma delimited sequence of boost library suffixes to search',
)

add_option('use-system-boost',
    help='use system version of boost libraries',
    nargs=0,
)

add_option('use-system-snappy',
    help='use system version of snappy library',
    nargs=0,
)

add_option('use-system-valgrind',
    help='use system version of valgrind library',
    nargs=0,
)

add_option('use-system-zlib',
    help='use system version of zlib library',
    nargs=0,
)

add_option('use-system-stemmer',
    help='use system version of stemmer',
    nargs=0)

add_option('use-system-yaml',
    help='use system version of yaml',
    nargs=0,
)

add_option('use-system-asio',
    help="use system version of ASIO",
    nargs=0,
)

add_option('use-system-icu',
    help="use system version of ICU",
    nargs=0,
)

add_option('use-system-intel_decimal128',
    help='use system version of intel decimal128',
    nargs=0,
)

add_option('use-system-all',
    help='use all system libraries',
    nargs=0,
)

add_option('use-new-tools',
    help='put new tools in the tarball',
    nargs=0,
)

add_option('build-mongoreplay',
    help='when building with --use-new-tools, build mongoreplay ( requires pcap dev )',
    nargs=1,
)

add_option('use-cpu-profiler',
    help='Link against the google-perftools profiler library',
    nargs=0,
)

add_option('build-fast-and-loose',
    choices=['on', 'off', 'auto'],
    const='on',
    default='auto',
    help='looser dependency checking',
    nargs='?',
    type='choice',
)

add_option('disable-warnings-as-errors',
    help="Don't add -Werror to compiler command line",
    nargs=0,
)

add_option('variables-help',
    help='Print the help text for SCons variables',
    nargs=0,
)

add_option('osx-version-min',
    help='minimum OS X version to support',
)

win_version_min_choices = {
    'vista'   : ('0600', '0000'),
    'win7'    : ('0601', '0000'),
    'ws08r2'  : ('0601', '0000'),
    'win8'    : ('0602', '0000'),
    'win81'   : ('0603', '0000'),
}

add_option('win-version-min',
    choices=win_version_min_choices.keys(),
    default=None,
    help='minimum Windows version to support',
    type='choice',
)

add_option('cache',
    choices=["all", "nolinked"],
    const='all',
    help='Use an object cache rather than a per-build variant directory (experimental)',
    nargs='?',
)

add_option('cache-dir',
    default='$BUILD_ROOT/scons/cache',
    help='Specify the directory to use for caching objects if --cache is in use',
)

add_option("cxx-std",
    choices=["11", "14"],
    default="11",
    help="Select the C++ langauge standard to build with",
)

def find_mongo_custom_variables():
    files = []
    for path in sys.path:
        probe = os.path.join(path, 'mongo_custom_variables.py')
        if os.path.isfile(probe):
            files.append(probe)
    return files

add_option('variables-files',
    default=find_mongo_custom_variables(),
    help="Specify variables files to load",
)

link_model_choices = ['auto', 'object', 'static', 'dynamic', 'dynamic-strict']
add_option('link-model',
    choices=link_model_choices,
    default='object',
    help='Select the linking model for the project',
    type='choice'
)

variable_parse_mode_choices=['auto', 'posix', 'other']
add_option('variable-parse-mode',
    choices=variable_parse_mode_choices,
    default=variable_parse_mode_choices[0],
    help='Select which parsing mode is used to interpret command line variables',
    type='choice',
)

add_option('modules',
    help="Comma-separated list of modules to build. Empty means none. Default is all.",
)

add_option('runtime-hardening',
    choices=["on", "off"],
    default="on",
    help="Enable runtime hardening features (e.g. stack smash protection)",
    type='choice',
)

add_option('use-s390x-crc32',
    choices=["on", "off"],
    default="on",
    help="Enable CRC32 hardware accelaration on s390x",
    type='choice',
)

add_option('git-decider',
    choices=["on", "off"],
    const='on',
    default="off",
    help="Use git metadata for out-of-date detection for source files",
    nargs='?',
    type="choice",
)

try:
    with open("version.json", "r") as version_fp:
        version_data = json.load(version_fp)

    if 'version' not in version_data:
        print "version.json does not contain a version string"
        Exit(1)
    if 'githash' not in version_data:
        version_data['githash'] = utils.getGitVersion()

except IOError as e:
    # If the file error wasn't because the file is missing, error out
    if e.errno != errno.ENOENT:
        print "Error opening version.json: {0}".format(e.strerror)
        Exit(1)

    version_data = {
        'version': utils.getGitDescribe()[1:],
        'githash': utils.getGitVersion(),
    }

except ValueError as e:
    print "Error decoding version.json: {0}".format(e)
    Exit(1)

# Setup the command-line variables
def variable_shlex_converter(val):
    # If the argument is something other than a string, propogate
    # it literally.
    if not isinstance(val, basestring):
        return val
    parse_mode = get_option('variable-parse-mode')
    if parse_mode == 'auto':
        parse_mode = 'other' if is_running_os('windows') else 'posix'
    return shlex.split(val, posix=(parse_mode == 'posix'))

def variable_arch_converter(val):
    arches = {
        'x86_64': 'x86_64',
        'amd64':  'x86_64',
        'emt64':   'x86_64',
        'x86':    'i386',
    }
    val = val.lower()

    if val in arches:
        return arches[val]

    # Uname returns a bunch of possible x86's on Linux.
    # Check whether the value is an i[3456]86 processor.
    if re.match(r'^i[3-6]86$', val):
        return 'i386'

    # Return whatever val is passed in - hopefully it's legit
    return val

# The Scons 'default' tool enables a lot of tools that we don't actually need to enable.
# On platforms like Solaris, it actually does the wrong thing by enabling the sunstudio
# toolchain first. As such it is simpler and more efficient to manually load the precise
# set of tools we need for each platform.
# If we aren't on a platform where we know the minimal set of tools, we fall back to loading
# the 'default' tool.
def decide_platform_tools():
    if is_running_os('windows'):
        # we only support MS toolchain on windows
        return ['msvc', 'mslink', 'mslib', 'masm']
    elif is_running_os('linux', 'solaris'):
        return ['gcc', 'g++', 'gnulink', 'ar', 'gas']
    elif is_running_os('osx'):
        return ['gcc', 'g++', 'applelink', 'ar', 'as']
    else:
        return ["default"]

def variable_tools_converter(val):
    tool_list = shlex.split(val)
    return tool_list + [
        "distsrc",
        "gziptool",
        "jsheader",
        "mergelib",
        "mongo_integrationtest",
        "mongo_unittest",
        "textfile",
    ]

def variable_distsrc_converter(val):
    if not val.endswith("/"):
        return val + "/"
    return val

variables_files = variable_shlex_converter(get_option('variables-files'))
for file in variables_files:
    print "Using variable customization file %s" % file

env_vars = Variables(
    files=variables_files,
    args=ARGUMENTS
)

env_vars.Add('ABIDW',
    help="Configures the path to the 'abidw' (a libabigail) utility")

env_vars.Add('ARFLAGS',
    help='Sets flags for the archiver',
    converter=variable_shlex_converter)

env_vars.Add(
    'CACHE_SIZE',
    help='Maximum size of the cache (in gigabytes)',
    default=32,
    converter=lambda x:int(x)
)

env_vars.Add(
    'CACHE_PRUNE_TARGET',
    help='Maximum percent in-use in cache after pruning',
    default=66,
    converter=lambda x:int(x)
)

env_vars.Add('CC',
    help='Select the C compiler to use')

env_vars.Add('CCFLAGS',
    help='Sets flags for the C and C++ compiler',
    converter=variable_shlex_converter)

env_vars.Add('CFLAGS',
    help='Sets flags for the C compiler',
    converter=variable_shlex_converter)

env_vars.Add('CPPDEFINES',
    help='Sets pre-processor definitions for C and C++',
    converter=variable_shlex_converter,
    default=[])

env_vars.Add('CPPPATH',
    help='Adds paths to the preprocessor search path',
    converter=variable_shlex_converter)

env_vars.Add('CXX',
    help='Select the C++ compiler to use')

env_vars.Add('CXXFLAGS',
    help='Sets flags for the C++ compiler',
    converter=variable_shlex_converter)

# Note: This probably is only really meaningful when configured via a variables file. It will
# also override whatever the SCons platform defaults would be.
env_vars.Add('ENV',
    help='Sets the environment for subprocesses')

env_vars.Add('HOST_ARCH',
    help='Sets the native architecture of the compiler',
    converter=variable_arch_converter,
    default=None)

env_vars.Add('LIBPATH',
    help='Adds paths to the linker search path',
    converter=variable_shlex_converter)

env_vars.Add('LIBS',
    help='Adds extra libraries to link against',
    converter=variable_shlex_converter)

env_vars.Add('LINKFLAGS',
    help='Sets flags for the linker',
    converter=variable_shlex_converter)

# Note: This is only really meaningful when configured via a variables file. See the
# default_buildinfo_environment_data() function for examples of how to use this.
env_vars.Add('MONGO_BUILDINFO_ENVIRONMENT_DATA',
    help='Sets the info returned from the buildInfo command and --version command-line flag',
    default=default_buildinfo_environment_data())

env_vars.Add('MONGO_DIST_SRC_PREFIX',
    help='Sets the prefix for files in the source distribution archive',
    converter=variable_distsrc_converter,
    default="mongodb-src-r${MONGO_VERSION}")

env_vars.Add('MONGO_DISTARCH',
    help='Adds a string representing the target processor architecture to the dist archive',
    default='$TARGET_ARCH')

env_vars.Add('MONGO_DISTMOD',
    help='Adds a string that will be embedded in the dist archive naming',
    default='')

env_vars.Add('MONGO_DISTNAME',
    help='Sets the version string to be used in dist archive naming',
    default='$MONGO_VERSION')

env_vars.Add('MONGO_VERSION',
    help='Sets the version string for MongoDB',
    default=version_data['version'])

env_vars.Add('MONGO_GIT_HASH',
    help='Sets the githash to store in the MongoDB version information',
    default=version_data['githash'])

env_vars.Add('MSVC_USE_SCRIPT',
    help='Sets the script used to setup Visual Studio.')

env_vars.Add('MSVC_VERSION',
    help='Sets the version of Visual Studio to use (e.g.  12.0, 11.0, 10.0)')

env_vars.Add('OBJCOPY',
    help='Sets the path to objcopy',
    default=WhereIs('objcopy'))

env_vars.Add('RPATH',
    help='Set the RPATH for dynamic libraries and executables',
    converter=variable_shlex_converter)

env_vars.Add('SHCCFLAGS',
    help='Sets flags for the C and C++ compiler when building shared libraries',
    converter=variable_shlex_converter)

env_vars.Add('SHCFLAGS',
    help='Sets flags for the C compiler when building shared libraries',
    converter=variable_shlex_converter)

env_vars.Add('SHCXXFLAGS',
    help='Sets flags for the C++ compiler when building shared libraries',
    converter=variable_shlex_converter)

env_vars.Add('SHLINKFLAGS',
    help='Sets flags for the linker when building shared libraries',
    converter=variable_shlex_converter)

env_vars.Add('TARGET_ARCH',
    help='Sets the architecture to build for',
    converter=variable_arch_converter,
    default=None)

env_vars.Add('TARGET_OS',
    help='Sets the target OS to build for',
    default=get_running_os_name())

env_vars.Add('TOOLS',
    help='Sets the list of SCons tools to add to the environment',
    converter=variable_tools_converter,
    default=decide_platform_tools())

env_vars.Add('VARIANT_DIR',
    help='Sets the name (or generator function) for the variant directory',
    default=default_variant_dir_generator,
)

env_vars.Add('VERBOSE',
    help='Control build verbosity (auto, on/off true/false 1/0)',
    default='auto',
)

env_vars.Add('WINDOWS_OPENSSL_BIN',
    help='Sets the path to the openssl binaries for packaging',
    default='c:/openssl/bin')

# -- Validate user provided options --

# A dummy environment that should *only* have the variables we have set. In practice it has
# some other things because SCons isn't quite perfect about keeping variable initialization
# scoped to Tools, but it should be good enough to do validation on any Variable values that
# came from the command line or from loaded files.
variables_only_env = Environment(
    # Disable platform specific variable injection
    platform=(lambda x: ()),
    # But do *not* load any tools, since those might actually set variables. Note that this
    # causes the value of our TOOLS variable to have no effect.
    tools=[],
    # Use the Variables specified above.
    variables=env_vars,
)

# don't run configure if user calls --help
if GetOption('help'):
    try:
        Help('\nThe following variables may also be set like scons VARIABLE=value\n', append=True)
        Help(env_vars.GenerateHelpText(variables_only_env), append=True)
    except TypeError:
        # The append=true kwarg is only supported in scons>=2.4. Without it, calls to Help() clobber
        # the automatically generated options help, which we don't want. Users on older scons
        # versions will need to use --variables-help to learn about which variables we support.
        pass

    Return()

if ('CC' in variables_only_env) != ('CXX' in variables_only_env):
    print('Cannot customize C compiler without customizing C++ compiler, and vice versa')
    Exit(1)

# --- environment setup ---

# If the user isn't using the # to indicate top-of-tree or $ to expand a variable, forbid
# relative paths. Relative paths don't really work as expected, because they end up relative to
# the top level SConstruct, not the invokers CWD. We could in theory fix this with
# GetLaunchDir, but that seems a step too far.
buildDir = get_option('build-dir').rstrip('/')
if buildDir[0] not in ['$', '#']:
    if not os.path.isabs(buildDir):
        print("Do not use relative paths with --build-dir")
        Exit(1)

cacheDir = get_option('cache-dir').rstrip('/')
if cacheDir[0] not in ['$', '#']:
    if not os.path.isabs(cacheDir):
        print("Do not use relative paths with --cache-dir")
        Exit(1)

installDir = get_option('prefix').rstrip('/')
if installDir[0] not in ['$', '#']:
    if not os.path.isabs(installDir):
        print("Do not use relative paths with --prefix")
        Exit(1)

sconsDataDir = Dir(buildDir).Dir('scons')
SConsignFile(str(sconsDataDir.File('sconsign')))

def printLocalInfo():
    import sys, SCons
    print( "scons version: " + SCons.__version__ )
    print( "python version: " + " ".join( [ `i` for i in sys.version_info ] ) )

printLocalInfo()

boostLibs = [ "thread" , "filesystem" , "program_options", "system", "regex", "chrono", "iostreams" ]

onlyServer = len( COMMAND_LINE_TARGETS ) == 0 or ( len( COMMAND_LINE_TARGETS ) == 1 and str( COMMAND_LINE_TARGETS[0] ) in [ "mongod" , "mongos" , "test" ] )

releaseBuild = has_option("release")

dbg_opt_mapping = {
    # --dbg, --opt   :   dbg    opt
    ( "on",  None  ) : ( True,  False ),  # special case interaction
    ( "on",  "on"  ) : ( True,  True ),
    ( "on",  "off" ) : ( True,  False ),
    ( "off", None  ) : ( False, True ),
    ( "off", "on"  ) : ( False, True ),
    ( "off", "off" ) : ( False, False ),
}
debugBuild, optBuild = dbg_opt_mapping[(get_option('dbg'), get_option('opt'))]

if releaseBuild and (debugBuild or not optBuild):
    print("Error: A --release build may not have debugging, and must have optimization")
    Exit(1)

noshell = has_option( "noshell" )

jsEngine = get_option( "js-engine")

serverJs = get_option( "server-js" ) == "on"

usemozjs = (jsEngine.startswith('mozjs'))

if not serverJs and not usemozjs:
    print("Warning: --server-js=off is not needed with --js-engine=none")

# We defer building the env until we have determined whether we want certain values. Some values
# in the env actually have semantics for 'None' that differ from being absent, so it is better
# to build it up via a dict, and then construct the Environment in one shot with kwargs.
#
# Yes, BUILD_ROOT vs BUILD_DIR is confusing. Ideally, BUILD_DIR would actually be called
# VARIANT_DIR, and at some point we should probably do that renaming. Until we do though, we
# also need an Environment variable for the argument to --build-dir, which is the parent of all
# variant dirs. For now, we call that BUILD_ROOT. If and when we s/BUILD_DIR/VARIANT_DIR/g,
# then also s/BUILD_ROOT/BUILD_DIR/g.
envDict = dict(BUILD_ROOT=buildDir,
               BUILD_DIR=make_variant_dir_generator(),
               DIST_ARCHIVE_SUFFIX='.tgz',
               DIST_BINARIES=[],
               MODULE_BANNERS=[],
               ARCHIVE_ADDITION_DIR_MAP={},
               ARCHIVE_ADDITIONS=[],
               PYTHON=utils.find_python(),
               SERVER_ARCHIVE='${SERVER_DIST_BASENAME}${DIST_ARCHIVE_SUFFIX}',
               UNITTEST_ALIAS='unittests',
               # TODO: Move unittests.txt to $BUILD_DIR, but that requires
               # changes to MCI.
               UNITTEST_LIST='$BUILD_ROOT/unittests.txt',
               INTEGRATION_TEST_ALIAS='integration_tests',
               INTEGRATION_TEST_LIST='$BUILD_ROOT/integration_tests.txt',
               CONFIGUREDIR=sconsDataDir.Dir('sconf_temp'),
               CONFIGURELOG=sconsDataDir.File('config.log'),
               INSTALL_DIR=installDir,
               CONFIG_HEADER_DEFINES={},
               LIBDEPS_TAG_EXPANSIONS=[],
               )

env = Environment(variables=env_vars, **envDict)
del envDict

env.AddMethod(env_os_is_wrapper, 'TargetOSIs')
env.AddMethod(env_get_os_name_wrapper, 'GetTargetOSName')

def fatal_error(env, msg, *args):
    print msg.format(*args)
    Exit(1)

def conf_error(env, msg, *args):
    print msg.format(*args)
    print "See {0} for details".format(env['CONFIGURELOG'].abspath)

    Exit(1)

env.AddMethod(fatal_error, 'FatalError')
env.AddMethod(conf_error, 'ConfError')

# Normalize the VERBOSE Option, and make its value available as a
# function.
if env['VERBOSE'] == "auto":
    env['VERBOSE'] = not sys.stdout.isatty()
elif env['VERBOSE'] in ('1', "ON", "on", "True", "true", True):
    env['VERBOSE'] = True
elif env['VERBOSE'] in ('0', "OFF", "off", "False", "false", False):
    env['VERBOSE'] = False
else:
    env.FatalError("Invalid value {0} for VERBOSE Variable", env['VERBOSE'])
env.AddMethod(lambda env: env['VERBOSE'], 'Verbose')

if has_option('variables-help'):
    print env_vars.GenerateHelpText(env)
    Exit(0)

#unknown_vars = env_vars.UnknownVariables()
#if unknown_vars:
#    env.FatalError("Unknown variables specified: {0}", ", ".join(unknown_vars.keys()))

def set_config_header_define(env, varname, varval = 1):
    env['CONFIG_HEADER_DEFINES'][varname] = varval
env.AddMethod(set_config_header_define, 'SetConfigHeaderDefine')

detectEnv = env.Clone()

# Identify the toolchain in use. We currently support the following:
# These macros came from
# http://nadeausoftware.com/articles/2012/10/c_c_tip_how_detect_compiler_name_and_version_using_compiler_predefined_macros
toolchain_macros = {
    'GCC': 'defined(__GNUC__) && !defined(__clang__)',
    'clang': 'defined(__clang__)',
    'MSVC': 'defined(_MSC_VER)'
}

def CheckForToolchain(context, toolchain, lang_name, compiler_var, source_suffix):
    test_body = textwrap.dedent("""
    #if {0}
    /* we are using toolchain {0} */
    #else
    #error
    #endif
    """.format(toolchain_macros[toolchain]))

    print_tuple = (lang_name, context.env[compiler_var], toolchain)
    context.Message('Checking if %s compiler "%s" is %s... ' % print_tuple)

    # Strip indentation from the test body to ensure that the newline at the end of the
    # endif is the last character in the file (rather than a line of spaces with no
    # newline), and that all of the preprocessor directives start at column zero. Both of
    # these issues can trip up older toolchains.
    result = context.TryCompile(test_body, source_suffix)
    context.Result(result)
    return result

endian = get_option( "endian" )

if endian == "auto":
    endian = sys.byteorder

if endian == "little":
    env.SetConfigHeaderDefine("MONGO_CONFIG_BYTE_ORDER", "1234")
elif endian == "big":
    env.SetConfigHeaderDefine("MONGO_CONFIG_BYTE_ORDER", "4321")

# These preprocessor macros came from
# http://nadeausoftware.com/articles/2012/02/c_c_tip_how_detect_processor_type_using_compiler_predefined_macros
#
# NOTE: Remember to add a trailing comma to form any required one
# element tuples, or your configure checks will fail in strange ways.
processor_macros = {
    'arm'     : { 'endian': 'little', 'defines': ('__arm__',) },
    'aarch64' : { 'endian': 'little', 'defines': ('__arm64__', '__aarch64__')},
    'i386'    : { 'endian': 'little', 'defines': ('__i386', '_M_IX86')},
    'ppc64le' : { 'endian': 'little', 'defines': ('__powerpc64__',)},
    's390x'   : { 'endian': 'big',    'defines': ('__s390x__',)},
    'sparc'   : { 'endian': 'big',    'defines': ('__sparc',)},
    'x86_64'  : { 'endian': 'little', 'defines': ('__x86_64', '_M_AMD64')},
}

def CheckForProcessor(context, which_arch):
    def run_compile_check(arch):
        full_macros = " || ".join([ "defined(%s)" % (v) for v in processor_macros[arch]['defines']])

        if not endian == processor_macros[arch]['endian']:
            return False

        test_body = """
        #if {0}
        /* Detected {1} */
        #else
        #error not {1}
        #endif
        """.format(full_macros, arch)

        return context.TryCompile(textwrap.dedent(test_body), ".c")

    if which_arch:
        ret = run_compile_check(which_arch)
        context.Message('Checking if target processor is %s ' % which_arch)
        context.Result(ret)
        return ret;

    for k in processor_macros.keys():
        ret = run_compile_check(k)
        if ret:
            context.Result('Detected a %s processor' % k)
            return k

    context.Result('Could not detect processor model/architecture')
    return False

# Taken from http://nadeausoftware.com/articles/2012/01/c_c_tip_how_use_compiler_predefined_macros_detect_operating_system
os_macros = {
    "windows": "_WIN32",
    "solaris": "__sun",
    "freebsd": "__FreeBSD__",
    "openbsd": "__OpenBSD__",
    "osx": "__APPLE__",
    "linux": "__linux__",
}

def CheckForOS(context, which_os):
    test_body = """
    #if defined({0})
    /* detected {1} */
    #else
    #error
    #endif
    """.format(os_macros[which_os], which_os)
    context.Message('Checking if target OS {0} is supported by the toolchain '.format(which_os))
    ret = context.TryCompile(textwrap.dedent(test_body), ".c")
    context.Result(ret)
    return ret

def CheckForCXXLink(context):
    test_body = """
    #include <iostream>
    #include <cstdlib>

    int main() {
        std::cout << "Hello, World" << std::endl;
        return EXIT_SUCCESS;
    }
    """
    context.Message('Checking that the C++ compiler can link a C++ program... ')
    ret = context.TryLink(textwrap.dedent(test_body), ".cpp")
    context.Result(ret)
    return ret

detectConf = Configure(detectEnv, help=False, custom_tests = {
    'CheckForToolchain' : CheckForToolchain,
    'CheckForProcessor': CheckForProcessor,
    'CheckForOS': CheckForOS,
    'CheckForCXXLink': CheckForCXXLink,
})

if not detectConf.CheckCC():
    env.ConfError(
        "C compiler {0} doesn't work",
        detectEnv['CC'])

if not detectConf.CheckCXX():
    env.ConfError(
        "C++ compiler {0} doesn't work",
        detectEnv['CXX'])

if not detectConf.CheckForCXXLink():
    env.ConfError(
        "C++ compiler {0} can't link C++ programs",
        detectEnv['CXX'])

toolchain_search_sequence = [ "GCC", "clang" ]
if is_running_os('windows'):
    toolchain_search_sequence = [ 'MSVC', 'clang', 'GCC' ]
for candidate_toolchain in toolchain_search_sequence:
    if detectConf.CheckForToolchain(candidate_toolchain, "C++", "CXX", ".cpp"):
        detected_toolchain = candidate_toolchain
        break

if not detected_toolchain:
    env.ConfError("Couldn't identify the C++ compiler")

if not detectConf.CheckForToolchain(detected_toolchain, "C", "CC", ".c"):
    env.ConfError("C compiler does not match identified C++ compiler")

# Now that we've detected the toolchain, we add methods to the env
# to get the canonical name of the toolchain and to test whether
# scons is using a particular toolchain.
def get_toolchain_name(self):
    return detected_toolchain.lower()
def is_toolchain(self, *args):
    actual_toolchain = self.ToolchainName()
    for v in args:
        if v.lower() == actual_toolchain:
            return True
    return False

env.AddMethod(get_toolchain_name, 'ToolchainName')
env.AddMethod(is_toolchain, 'ToolchainIs')

if env['TARGET_ARCH']:
    if not detectConf.CheckForProcessor(env['TARGET_ARCH']):
        env.ConfError("Could not detect processor specified in TARGET_ARCH variable")
else:
    detected_processor = detectConf.CheckForProcessor(None)
    if not detected_processor:
        env.ConfError("Failed to detect a supported target architecture")
    env['TARGET_ARCH'] = detected_processor

if env['TARGET_OS'] not in os_macros:
    print "No special config for [{0}] which probably means it won't work".format(env['TARGET_OS'])
elif not detectConf.CheckForOS(env['TARGET_OS']):
    env.ConfError("TARGET_OS ({0}) is not supported by compiler", env['TARGET_OS'])

detectConf.Finish()

env['CC_VERSION'] = get_toolchain_ver(env, 'CC')
env['CXX_VERSION'] = get_toolchain_ver(env, 'CXX')

if not env['HOST_ARCH']:
    env['HOST_ARCH'] = env['TARGET_ARCH']

# In some places we have POSIX vs Windows cpp files, and so there's an additional
# env variable to interpolate their names in child sconscripts

env['TARGET_OS_FAMILY'] = 'posix' if env.TargetOSIs('posix') else env.GetTargetOSName()

# Currently we only use tcmalloc on windows and linux x86_64. Other
# linux targets (power, s390x, arm) do not currently support tcmalloc.
#
# Normalize the allocator option and store it in the Environment. It
# would be nicer to use SetOption here, but you can't reset user
# options for some strange reason in SCons. Instead, we store this
# option as a new variable in the environment.
if get_option('allocator') == "auto":
    if env.TargetOSIs('windows') or \
       env.TargetOSIs('linux'):
        env['MONGO_ALLOCATOR'] = "tcmalloc"
    else:
        env['MONGO_ALLOCATOR'] = "system"
else:
    env['MONGO_ALLOCATOR'] = get_option('allocator')

if has_option("cache"):
    if has_option("gcov"):
        env.FatalError("Mixing --cache and --gcov doesn't work correctly yet. See SERVER-11084")
    env.CacheDir(str(env.Dir(cacheDir)))

    if get_option("cache") == "nolinked":
        def noCacheEmitter(target, source, env):
            for t in target:
                env.NoCache(t)
            return target, source

        def addNoCacheEmitter(builder):
            origEmitter = builder.emitter
            if SCons.Util.is_Dict(origEmitter):
                for k,v in origEmitter:
                    origEmitter[k] = SCons.Builder.ListEmitter([v, noCacheEmitter])
            elif SCons.Util.is_List(origEmitter):
                emitter.append(noCacheEmitter)
            else:
                builder.emitter = SCons.Builder.ListEmitter([origEmitter, noCacheEmitter])

        addNoCacheEmitter(env['BUILDERS']['Program'])
        addNoCacheEmitter(env['BUILDERS']['StaticLibrary'])
        addNoCacheEmitter(env['BUILDERS']['SharedLibrary'])
        addNoCacheEmitter(env['BUILDERS']['LoadableModule'])

# Normalize the link model. If it is auto, then a release build uses 'object' mode. Otherwise
# we automatically select the 'static' model on non-windows platforms, or 'object' if on
# Windows. If the user specified, honor the request, unless it conflicts with the requirement
# that release builds use the 'object' mode, in which case, error out.
#
# We require the use of the 'object' mode for release builds because it is the only linking
# model that works across all of our platforms. We would like to ensure that all of our
# released artifacts are built with the same known-good-everywhere model.
link_model = get_option('link-model')

if link_model == "auto":
    link_model = "object" if (env.TargetOSIs('windows') or has_option("release")) else "static"
elif has_option("release") and link_model != "object":
    env.FatalError("The link model for release builds is required to be 'object'")

# The only link model currently supported on Windows is 'object', since there is no equivalent
# to --whole-archive.
if env.TargetOSIs('windows') and link_model != 'object':
    env.FatalError("Windows builds must use the 'object' link model");

# The 'object' mode for libdeps is enabled by setting _LIBDEPS to $_LIBDEPS_OBJS. The other two
# modes operate in library mode, enabled by setting _LIBDEPS to $_LIBDEPS_LIBS.
env['_LIBDEPS'] = '$_LIBDEPS_OBJS' if link_model == "object" else '$_LIBDEPS_LIBS'

env['BUILDERS']['ProgramObject'] = env['BUILDERS']['StaticObject']
env['BUILDERS']['LibraryObject'] = env['BUILDERS']['StaticObject']

if link_model.startswith("dynamic"):

    # Add in the abi linking tool if the user requested and it is
    # supported on this platform.
    if env.get('ABIDW'):
        abilink = Tool('abilink')
        if abilink.exists(env):
            abilink(env)

    # Redirect the 'Library' target, which we always use instead of 'StaticLibrary' for things
    # that can be built in either mode, to point to SharedLibrary.
    env['BUILDERS']['Library'] = env['BUILDERS']['SharedLibrary']
    env['BUILDERS']['LibraryObject'] = env['BUILDERS']['SharedObject']

    # TODO: Ideally, the conditions below should be based on a
    # detection of what linker we are using, not the local OS, but I
    # doubt very much that we will see the mach-o linker on anything
    # other than Darwin, or a BFD/sun-esque linker elsewhere.

    # On Darwin, we need to tell the linker that undefined symbols are
    # resolved via dynamic lookup; otherwise we get build failures. On
    # other unixes, we need to suppress as-needed behavior so that
    # initializers are ensured present, even if there is no visible
    # edge to the library in the symbol graph.
    #
    # NOTE: The darwin linker flag is only needed because the library
    # graph is not a DAG. Once the graph is a DAG, we will require all
    # edges to be expressed, and we should drop the flag. When that
    # happens, we should also add -z,defs flag on ELF platforms to
    # ensure that missing symbols due to unnamed dependency edges
    # result in link errors.
    #
    # NOTE: The 'incomplete' tag can be applied to a library to
    # indicate that it does not (or cannot) completely express all of
    # its required link dependencies. This can occur for three
    # reasons:
    #
    # - No unique provider for the symbol: Some symbols do not have a
    #   unique dependency that provides a definition, in which case it
    #   is impossible for the library to express a dependency edge to
    #   resolve the symbol
    #
    # - The library is part of a cycle: If library A depends on B,
    #   which depends on C, which depends on A, then it is impossible
    #   to express all three edges in SCons, since otherwise there is
    #   no way to sequence building the libraries. The cyclic
    #   libraries actually work at runtime, because some parent object
    #   links all of them.
    #
    # - The symbol is provided by an executable into which the library
    #   will be linked. The mongo::inShutdown symbol is a good
    #   example.
    #
    # All of these are defects in the linking model. In an effort to
    # eliminate these issues, we have begun tagging those libraries
    # that are affected, and requiring that all non-tagged libraries
    # correctly express all dependencies. As we repair each defective
    # library, we can remove the tag. When all the tags are removed
    # the graph will be acyclic.

    if env.TargetOSIs('osx'):
        if link_model == "dynamic-strict":
            # Darwin is strict by default
            pass
        else:
            def libdeps_tags_expand_incomplete(source, target, env, for_signature):
                # On darwin, since it is strict by default, we need to add a flag
                # when libraries are tagged incomplete.
                if 'incomplete' in target[0].get_env().get("LIBDEPS_TAGS", []):
                    return ["-Wl,-undefined,dynamic_lookup"]
                return []
            env['LIBDEPS_TAG_EXPANSIONS'].append(libdeps_tags_expand_incomplete)
    else:
        env.AppendUnique(LINKFLAGS=["-Wl,--no-as-needed"])

        # Using zdefs doesn't work at all with the sanitizers
        if not has_option('sanitize'):

            if link_model == "dynamic-strict":
                env.AppendUnique(SHLINKFLAGS=["-Wl,-z,defs"])
            else:
                # On BFD/gold linker environments, which are not strict by
                # default, we need to add a flag when libraries are not
                # tagged incomplete.
                def libdeps_tags_expand_incomplete(source, target, env, for_signature):
                    if 'incomplete' not in target[0].get_env().get("LIBDEPS_TAGS", []):
                        return ["-Wl,-z,defs"]
                    return []
                env['LIBDEPS_TAG_EXPANSIONS'].append(libdeps_tags_expand_incomplete)

if optBuild:
    env.SetConfigHeaderDefine("MONGO_CONFIG_OPTIMIZED_BUILD")

# Enable the fast decider if exlicltly requested or if in 'auto' mode and not in conflict with other
# options.
if get_option('build-fast-and-loose') == 'on' or \
   (get_option('build-fast-and-loose') == 'auto' and \
    not has_option('release') and \
    not has_option('cache')):
    # See http://www.scons.org/wiki/GoFastButton for details
    env.Decider('MD5-timestamp')
    env.SetOption('max_drift', 1)

# If the user has requested the git decider, enable it if it is available. We want to do this after
# we set the basic decider above, so that we 'chain' to that one.
if get_option('git-decider') == 'on':
    git_decider = Tool('git_decider')
    if git_decider.exists(env):
        git_decider(env)

# On non-windows platforms, we may need to differentiate between flags being used to target an
# executable (like -fPIE), vs those being used to target a (shared) library (like -fPIC). To do so,
# we inject a new family of SCons variables PROG*FLAGS, by reaching into the various COMs.
if not env.TargetOSIs('windows'):
    env["CCCOM"] = env["CCCOM"].replace("$CFLAGS", "$CFLAGS $PROGCFLAGS")
    env["CXXCOM"] = env["CXXCOM"].replace("$CXXFLAGS", "$CXXFLAGS $PROGCXXFLAGS")
    env["CCCOM"] = env["CCCOM"].replace("$CCFLAGS", "$CCFLAGS $PROGCCFLAGS")
    env["CXXCOM"] = env["CXXCOM"].replace("$CCFLAGS", "$CCFLAGS $PROGCCFLAGS")
    env["LINKCOM"] = env["LINKCOM"].replace("$LINKFLAGS", "$LINKFLAGS $PROGLINKFLAGS")

if not env.Verbose():
    env.Append( CCCOMSTR = "Compiling $TARGET" )
    env.Append( CXXCOMSTR = env["CCCOMSTR"] )
    env.Append( SHCCCOMSTR = "Compiling $TARGET" )
    env.Append( SHCXXCOMSTR = env["SHCCCOMSTR"] )
    env.Append( LINKCOMSTR = "Linking $TARGET" )
    env.Append( SHLINKCOMSTR = env["LINKCOMSTR"] )
    env.Append( ARCOMSTR = "Generating library $TARGET" )

if env['_LIBDEPS'] == '$_LIBDEPS_OBJS':
    # The libraries we build in LIBDEPS_OBJS mode are just placeholders for tracking dependencies.
    # This avoids wasting time and disk IO on them.
    def write_uuid_to_file(env, target, source):
        with open(env.File(target[0]).abspath, 'w') as fake_lib:
            fake_lib.write(str(uuid.uuid4()))
            fake_lib.write('\n')

    def noop_action(env, target, source):
        pass

    env['ARCOM'] = write_uuid_to_file
    env['ARCOMSTR'] = 'Generating placeholder library $TARGET'
    env['RANLIBCOM'] = noop_action
    env['RANLIBCOMSTR'] = 'Skipping ranlib for $TARGET'
elif env['_LIBDEPS'] == '$_LIBDEPS_LIBS':
    env.Tool('thin_archive')


libdeps.setup_environment(env, emitting_shared=(link_model.startswith("dynamic")))

if env.TargetOSIs('linux', 'freebsd', 'openbsd'):
    env['LINK_LIBGROUP_START'] = '-Wl,--start-group'
    env['LINK_LIBGROUP_END'] = '-Wl,--end-group'
    env['LINK_WHOLE_ARCHIVE_START'] = '-Wl,--whole-archive'
    env['LINK_WHOLE_ARCHIVE_END'] = '-Wl,--no-whole-archive'
elif env.TargetOSIs('osx'):
    env['LINK_LIBGROUP_START'] = ''
    env['LINK_LIBGROUP_END'] = ''
    env['LINK_WHOLE_ARCHIVE_START'] = '-Wl,-all_load'
    env['LINK_WHOLE_ARCHIVE_END'] = '-Wl,-noall_load'
elif env.TargetOSIs('solaris'):
    env['LINK_LIBGROUP_START'] = '-z rescan-start'
    env['LINK_LIBGROUP_END'] = '-z rescan-end'
    env['LINK_WHOLE_ARCHIVE_START'] = '-z allextract'
    env['LINK_WHOLE_ARCHIVE_END'] = '-z defaultextract'

# ---- other build setup -----
if debugBuild:
    env.SetConfigHeaderDefine("MONGO_CONFIG_DEBUG_BUILD")
else:
    env.AppendUnique( CPPDEFINES=[ 'NDEBUG' ] )

if env.TargetOSIs('linux'):
    env.Append( LIBS=['m'] )

elif env.TargetOSIs('solaris'):
     env.Append( LIBS=["socket","resolv","lgrp"] )

elif env.TargetOSIs('freebsd'):
    env.Append( LIBS=[ "kvm" ] )
    env.Append( CCFLAGS=[ "-fno-omit-frame-pointer" ] )

elif env.TargetOSIs('openbsd'):
    env.Append( LIBS=[ "kvm" ] )

elif env.TargetOSIs('windows'):
    dynamicCRT = has_option("dynamic-windows")

    env['DIST_ARCHIVE_SUFFIX'] = '.zip'

    # If tools configuration fails to set up 'cl' in the path, fall back to importing the whole
    # shell environment and hope for the best. This will work, for instance, if you have loaded
    # an SDK shell.
    for pathdir in env['ENV']['PATH'].split(os.pathsep):
        if os.path.exists(os.path.join(pathdir, 'cl.exe')):
            break
    else:
        print("NOTE: Tool configuration did not find 'cl' compiler, falling back to os environment")
        env['ENV'] = dict(os.environ)

    env.Append(CPPDEFINES=[
    # This tells the Windows compiler not to link against the .lib files
    # and to use boost as a bunch of header-only libraries
        "BOOST_ALL_NO_LIB",
    ])

    env.Append( CPPDEFINES=[ "_UNICODE" ] )
    env.Append( CPPDEFINES=[ "UNICODE" ] )

    # /EHsc exception handling style for visual studio
    # /W3 warning level
    env.Append(CCFLAGS=["/EHsc","/W3"])

    # some warnings we don't like:
    # c4355
    # 'this' : used in base member initializer list
    #    The this pointer is valid only within nonstatic member functions. It cannot be used in the initializer list for a base class.
    # c4800
    # 'type' : forcing value to bool 'true' or 'false' (performance warning)
    #    This warning is generated when a value that is not bool is assigned or coerced into type bool. 
    # c4267
    # 'var' : conversion from 'size_t' to 'type', possible loss of data
    # When compiling with /Wp64, or when compiling on a 64-bit operating system, type is 32 bits but size_t is 64 bits when compiling for 64-bit targets. To fix this warning, use size_t instead of a type.
    # c4244
    # 'conversion' conversion from 'type1' to 'type2', possible loss of data
    #  An integer type is converted to a smaller integer type.
    # c4290
    #  C++ exception specification ignored except to indicate a function is not __declspec(nothrow
    #  A function is declared using exception specification, which Visual C++ accepts but does not
    #  implement
    # c4068
    #  unknown pragma -- added so that we can specify unknown pragmas for other compilers
    # c4351
    #  on extremely old versions of MSVC (pre 2k5), default constructing an array member in a
    #  constructor's initialization list would not zero the array members "in some cases".
    #  since we don't target MSVC versions that old, this warning is safe to ignore.
    env.Append( CCFLAGS=["/wd4355", "/wd4800", "/wd4267", "/wd4244",
                         "/wd4290", "/wd4068", "/wd4351"] )

    # some warnings we should treat as errors:
    # c4013
    #  'function' undefined; assuming extern returning int
    #    This warning occurs when files compiled for the C language use functions not defined
    #    in a header file.
    # c4099
    #  identifier' : type name first seen using 'objecttype1' now seen using 'objecttype2'
    #    This warning occurs when classes and structs are declared with a mix of struct and class
    #    which can cause linker failures
    # c4930
    #  'identifier': prototyped function not called (was a variable definition intended?)
    #     This warning indicates a most-vexing parse error, where a user declared a function that
    #     was probably intended as a variable definition.  A common example is accidentally
    #     declaring a function called lock that takes a mutex when one meant to create a guard
    #     object called lock on the stack.
    env.Append( CCFLAGS=["/we4013", "/we4099", "/we4930"] )

    env.Append( CPPDEFINES=["_CONSOLE","_CRT_SECURE_NO_WARNINGS"] )

    # this would be for pre-compiled headers, could play with it later  
    #env.Append( CCFLAGS=['/Yu"pch.h"'] )

    # docs say don't use /FD from command line (minimal rebuild)
    # /Gy function level linking (implicit when using /Z7)
    # /Z7 debug info goes into each individual .obj file -- no .pdb created 
    env.Append( CCFLAGS= ["/Z7", "/errorReport:none"] )

    # /DEBUG will tell the linker to create a .pdb file
    # which WinDbg and Visual Studio will use to resolve
    # symbols if you want to debug a release-mode image.
    # Note that this means we can't do parallel links in the build.
    #
    # Please also note that this has nothing to do with _DEBUG or optimization.
    env.Append( LINKFLAGS=["/DEBUG"] )

    # /MD:  use the multithreaded, DLL version of the run-time library (MSVCRT.lib/MSVCR###.DLL)
    # /MT:  use the multithreaded, static version of the run-time library (LIBCMT.lib)
    # /MDd: Defines _DEBUG, _MT, _DLL, and uses MSVCRTD.lib/MSVCRD###.DLL
    # /MTd: Defines _DEBUG, _MT, and causes your application to use the
    #       debug multithread version of the run-time library (LIBCMTD.lib)

    winRuntimeLibMap = {
          #dyn   #dbg
        ( False, False ) : "/MT",
        ( False, True  ) : "/MTd",
        ( True,  False ) : "/MD",
        ( True,  True  ) : "/MDd",
    }

    env.Append(CCFLAGS=[winRuntimeLibMap[(dynamicCRT, debugBuild)]])

    if optBuild:
        # /O2:  optimize for speed (as opposed to size)
        # /Oy-: disable frame pointer optimization (overrides /O2, only affects 32-bit)
        # /INCREMENTAL: NO - disable incremental link - avoid the level of indirection for function
        # calls
        env.Append( CCFLAGS=["/O2", "/Oy-"] )
        env.Append( LINKFLAGS=["/INCREMENTAL:NO"])
    else:
        env.Append( CCFLAGS=["/Od"] )

    if debugBuild and not optBuild:
        # /RTC1: - Enable Stack Frame Run-Time Error Checking; Reports when a variable is used
        # without having been initialized (implies /Od: no optimizations)
        env.Append( CCFLAGS=["/RTC1"] )

    # Support large object files since some unit-test sources contain a lot of code
    env.Append( CCFLAGS=["/bigobj"] )

    # This gives 32-bit programs 4 GB of user address space in WOW64, ignored in 64-bit builds
    env.Append( LINKFLAGS=["/LARGEADDRESSAWARE"] )

    env.Append(
        LIBS=[
            'DbgHelp.lib',
            'Iphlpapi.lib',
            'Psapi.lib',
            'advapi32.lib',
            'bcrypt.lib',
            'crypt32.lib',
            'kernel32.lib',
            'shell32.lib',
            'pdh.lib',
            'version.lib',
            'winmm.lib',
            'ws2_32.lib',
        ],
    )

# When building on visual studio, this sets the name of the debug symbols file
if env.ToolchainIs('msvc'):
    env['PDB'] = '${TARGET.base}.pdb'

if env.TargetOSIs('posix'):

    # Everything on OS X is position independent by default. Solaris doesn't support PIE.
    if not env.TargetOSIs('osx', 'solaris'):
        if get_option('runtime-hardening') == "on":
            # If runtime hardening is requested, then build anything
            # destined for an executable with the necessary flags for PIE.
            env.AppendUnique(
                PROGCCFLAGS=['-fPIE'],
                PROGLINKFLAGS=['-pie'],
            )

    # -Winvalid-pch Warn if a precompiled header (see Precompiled Headers) is found in the search path but can't be used.
    env.Append( CCFLAGS=["-fno-omit-frame-pointer",
                         "-fno-strict-aliasing",
                         "-ggdb",
                         "-pthread",
                         "-Wall",
                         "-Wsign-compare",
                         "-Wno-unknown-pragmas",
                         "-Winvalid-pch"] )

    # env.Append( " -Wconversion" ) TODO: this doesn't really work yet
    if env.TargetOSIs('linux', 'osx', 'solaris'):
        if not has_option("disable-warnings-as-errors"):
            env.Append( CCFLAGS=["-Werror"] )

    env.Append( CXXFLAGS=["-Woverloaded-virtual"] )
    env.Append( LINKFLAGS=["-pthread"] )

    # SERVER-9761: Ensure early detection of missing symbols in dependent libraries at program
    # startup.
    if env.TargetOSIs('osx'):
        env.Append( LINKFLAGS=["-Wl,-bind_at_load"] )
    else:
        env.Append( LINKFLAGS=["-Wl,-z,now"] )
        env.Append( LINKFLAGS=["-rdynamic"] )

    env.Append( LIBS=[] )

    #make scons colorgcc friendly
    for key in ('HOME', 'TERM'):
        try:
            env['ENV'][key] = os.environ[key]
        except KeyError:
            pass

    if env.TargetOSIs('linux') and has_option( "gcov" ):
        env.Append( CCFLAGS=["-fprofile-arcs", "-ftest-coverage"] )
        env.Append( LINKFLAGS=["-fprofile-arcs", "-ftest-coverage"] )

    if optBuild:
        env.Append( CCFLAGS=["-O2"] )
    else:
        env.Append( CCFLAGS=["-O0"] )

    # Promote linker warnings into errors. We can't yet do this on OS X because its linker considers
    # noall_load obsolete and warns about it.
    if not env.TargetOSIs('osx'):
        env.Append(
            LINKFLAGS=[
                "-Wl,--fatal-warnings",
            ],
        )

mmapv1 = False
if get_option('mmapv1') == 'auto':
    # The mmapv1 storage engine is only supported on x86
    # targets. Unless explicitly requested, disable it on all other
    # platforms.
    mmapv1 = (env['TARGET_ARCH'] in ['i386', 'x86_64'])
elif get_option('mmapv1') == 'on':
    mmapv1 = True

wiredtiger = False
if get_option('wiredtiger') == 'on':
    # Wiredtiger only supports 64-bit architecture, and will fail to compile on 32-bit
    # so disable WiredTiger automatically on 32-bit since wiredtiger is on by default
    if env['TARGET_ARCH'] == 'i386':
        env.FatalError("WiredTiger is not supported on 32-bit platforms\n"
            "Re-run scons with --wiredtiger=off to build on 32-bit platforms")
    else:
        wiredtiger = True
        env.SetConfigHeaderDefine("MONGO_CONFIG_WIREDTIGER_ENABLED")

if env['TARGET_ARCH'] == 'i386':
    # If we are using GCC or clang to target 32 bit, set the ISA minimum to 'nocona',
    # and the tuning to 'generic'. The choice of 'nocona' is selected because it
    #  -- includes MMX extenions which we need for tcmalloc on 32-bit
    #  -- can target 32 bit
    #  -- is at the time of this writing a widely-deployed 10 year old microarchitecture
    #  -- is available as a target architecture from GCC 4.0+
    # However, we only want to select an ISA, not the nocona specific scheduling, so we
    # select the generic tuning. For installations where hardware and system compiler rev are
    # contemporaries, the generic scheduling should be appropriate for a wide range of
    # deployed hardware.

    if env.ToolchainIs('GCC', 'clang'):
        env.Append( CCFLAGS=['-march=nocona', '-mtune=generic'] )

# Needed for auth tests since key files are stored in git with mode 644.
if not env.TargetOSIs('windows'):
    for keysuffix in [ "1" , "2" ]:
        keyfile = "jstests/libs/key%s" % keysuffix
        os.chmod( keyfile , stat.S_IWUSR|stat.S_IRUSR )

# boostSuffixList is used when using system boost to select a search sequence
# for boost libraries.
boostSuffixList = ["-mt", ""]
if get_option("system-boost-lib-search-suffixes") is not None:
    if not use_system_version_of_library("boost"):
        env.FatalError("The --system-boost-lib-search-suffixes option is only valid "
            "with --use-system-boost")
    boostSuffixList = get_option("system-boost-lib-search-suffixes")
    if boostSuffixList == "":
        boostSuffixList = []
    else:
        boostSuffixList = boostSuffixList.split(',')

# discover modules, and load the (python) module for each module's build.py
mongo_modules = moduleconfig.discover_modules('src/mongo/db/modules', get_option('modules'))
env['MONGO_MODULES'] = [m.name for m in mongo_modules]

# --- check system ---

def doConfigure(myenv):
    global wiredtiger

    def CheckStorageClass(context, storage_class):
        test_body = """
        {0} int tsp_int = 1;
        int main(int argc, char** argv) {{
            return !(tsp_int == argc);
        }}
        """.format(storage_class)
        context.Message('Checking for storage class {0} '.format(storage_class))
        ret = context.TryLink(textwrap.dedent(test_body), ".cpp")
        #ret = context.TryCompile(textwrap.dedent(test_body), ".cpp")
        context.Result(ret)
        return ret

    conf = Configure(myenv, help=False, custom_tests = {
        'CheckStorageClass': CheckStorageClass
    })
    haveTriviallyConstructibleThreadLocals = False
    for storage_class, macro_name in [
            ('thread_local', 'MONGO_CONFIG_HAVE_THREAD_LOCAL'),
            ('__thread', 'MONGO_CONFIG_HAVE___THREAD'),
            ('__declspec(thread)', 'MONGO_CONFIG_HAVE___DECLSPEC_THREAD')]:
        if conf.CheckStorageClass(storage_class):
            haveTriviallyConstructibleThreadLocals = True
            myenv.SetConfigHeaderDefine(macro_name)
    conf.Finish()
    if not haveTriviallyConstructibleThreadLocals:
        env.ConfError("Compiler must support a thread local storage class for trivially constructible types")

env = doConfigure( env )
